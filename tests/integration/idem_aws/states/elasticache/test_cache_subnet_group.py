import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_cache_subnet_group(hub, ctx, aws_ec2_subnet):
    # Create cache subnet group
    cache_subnet_group_name = "idem-test-cache-subnet-" + str(uuid.uuid4())
    cache_subnet_group_description = "idem-test-cache-subnet-description"
    tags = [{"Key": "Name", "Value": cache_subnet_group_name}]

    # Create elasticache subnet group with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.elasticache.cache_subnet_group.present(
        test_ctx,
        name=cache_subnet_group_name,
        cache_subnet_group_description=cache_subnet_group_description,
        subnet_ids=[
            aws_ec2_subnet.get("SubnetId"),
        ],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.elasticache.cache_subnet_group",
            name=cache_subnet_group_name,
        )[0]
        in ret["comment"]
    )
    assert not ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert cache_subnet_group_name == resource.get("name")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, resource.get("tags")
    )
    # Localstack pro does not populate subnets in the output
    if not hub.tool.utils.is_running_localstack(ctx):
        assert hub.tool.aws.state_comparison_utils.are_lists_identical(
            [aws_ec2_subnet.get("SubnetId")], resource.get("subnet_ids")
        )

    # Create real elasticache subnet group
    ret = await hub.states.aws.elasticache.cache_subnet_group.present(
        ctx,
        name=cache_subnet_group_name,
        cache_subnet_group_description=cache_subnet_group_description,
        subnet_ids=[
            aws_ec2_subnet.get("SubnetId"),
        ],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.elasticache.cache_subnet_group",
            name=cache_subnet_group_name,
        )[0]
        in ret["comment"]
    )
    assert not ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert cache_subnet_group_name == resource.get("name")
    assert cache_subnet_group_description == resource.get(
        "cache_subnet_group_description"
    )
    # Localstack pro does not populate subnets in the output
    if not hub.tool.utils.is_running_localstack(ctx):
        assert hub.tool.aws.state_comparison_utils.are_lists_identical(
            [aws_ec2_subnet.get("SubnetId")], resource.get("subnet_ids")
        )
        assert hub.tool.aws.state_comparison_utils.are_lists_identical(
            tags, resource.get("tags")
        )

    # Describe cache subnet group
    describe_ret = await hub.states.aws.elasticache.cache_subnet_group.describe(ctx)
    if not hub.tool.utils.is_running_localstack(ctx):
        assert resource_id in describe_ret
        # Verify that the describe output format is correct
        assert "aws.elasticache.cache_subnet_group.present" in describe_ret.get(
            resource_id
        )
        described_resource = describe_ret.get(resource_id).get(
            "aws.elasticache.cache_subnet_group.present"
        )
        described_resource_map = dict(ChainMap(*described_resource))
        if described_resource_map.get("tags") is not None:
            assert hub.tool.aws.state_comparison_utils.are_lists_identical(
                tags, described_resource_map.get("tags")
            )

    cache_subnet_group_description = "idem-test-cache-subnet-description-update"
    new_tags = [{"Key": "Name_Update", "Value": cache_subnet_group_name}]
    # update cache subnet group with test flag
    ret = await hub.states.aws.elasticache.cache_subnet_group.present(
        test_ctx,
        name=cache_subnet_group_name,
        cache_subnet_group_description=cache_subnet_group_description,
        resource_id=resource_id,
        subnet_ids=[
            aws_ec2_subnet.get("SubnetId"),
        ],
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert cache_subnet_group_name == resource.get("name")
    assert cache_subnet_group_description == resource.get(
        "cache_subnet_group_description"
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        new_tags, resource.get("tags")
    )
    # Localstack pro does not populate subnets in the output
    if not hub.tool.utils.is_running_localstack(ctx):
        assert hub.tool.aws.state_comparison_utils.are_lists_identical(
            [aws_ec2_subnet.get("SubnetId")], resource.get("subnet_ids")
        )

    # update cache subnet group
    ret = await hub.states.aws.elasticache.cache_subnet_group.present(
        ctx,
        name=cache_subnet_group_name,
        cache_subnet_group_description=cache_subnet_group_description,
        resource_id=resource_id,
        subnet_ids=[
            aws_ec2_subnet.get("SubnetId"),
        ],
        tags=new_tags,
    )
    assert ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert cache_subnet_group_name == resource.get("name")
    assert cache_subnet_group_description == resource.get(
        "cache_subnet_group_description"
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        new_tags, resource.get("tags")
    )
    # Localstack pro does not populate subnets in the output
    if not hub.tool.utils.is_running_localstack(ctx):
        assert hub.tool.aws.state_comparison_utils.are_lists_identical(
            [aws_ec2_subnet.get("SubnetId")], resource.get("subnet_ids")
        )

    # Delete cache subnet group with test flag
    ret = await hub.states.aws.elasticache.cache_subnet_group.absent(
        test_ctx, name=cache_subnet_group_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.elasticache.cache_subnet_group",
            name=cache_subnet_group_name,
        )[0]
        in ret["comment"]
    )

    # Trying to delete cache subnet group without resource_id should result in no-op.
    ret = await hub.states.aws.elasticache.cache_subnet_group.absent(
        test_ctx,
        name=cache_subnet_group_name,
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.elasticache.cache_subnet_group",
            name=cache_subnet_group_name,
        )[0]
        in ret["comment"]
    )

    # Delete cache subnet group
    ret = await hub.states.aws.elasticache.cache_subnet_group.absent(
        ctx, name=cache_subnet_group_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.elasticache.cache_subnet_group",
            name=cache_subnet_group_name,
        )[0]
        in ret["comment"]
    )
    assert ret["old_state"] and not ret["new_state"]

    # Deleting the same instance again (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.elasticache.cache_subnet_group.absent(
        ctx, name=cache_subnet_group_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.elasticache.cache_subnet_group",
            name=cache_subnet_group_name,
        )[0]
        in ret["comment"]
    )
