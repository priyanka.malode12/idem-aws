import copy
import uuid
from collections import ChainMap

import pytest


# Localstack currently has limited support for EFS file system. tests added to run with actual AWS services


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_file_system(hub, ctx):
    # Skip tests for localstack
    if hub.tool.utils.is_running_localstack(ctx):
        return

    # Init
    creation_token = str(uuid.uuid4())
    file_system_name = "idem-test-fs-" + creation_token
    resource_id = None
    performance_mode = "generalPurpose"
    encrypted = False
    throughput_mode = "bursting"
    provisioned_throughput_in_mibps = 500.0
    availability_zone_name = ctx["acct"].get("region_name") + "b"
    backup = False
    tags = [{"Key": "Name", "Value": file_system_name}]

    updated_backup = True
    updated_throughput_mode = "provisioned"
    updated_tags = [
        {"Key": "Name", "Value": file_system_name},
        {"Key": "id", "Value": "fs-1"},
    ]
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Test-run file system create
    ret = await hub.states.aws.efs.file_system.present(
        test_ctx,
        name=file_system_name,
        creation_token=creation_token,
        resource_id=resource_id,
        performance_mode=performance_mode,
        encrypted=encrypted,
        throughput_mode=throughput_mode,
        provisioned_throughput_in_mibps=provisioned_throughput_in_mibps,
        availability_zone_name=availability_zone_name,
        backup=backup,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (f"Would create aws.efs.file_system '{file_system_name}'",)
    resource = ret.get("new_state")

    assert creation_token == resource["creation_token"]
    assert performance_mode == resource["performance_mode"]
    assert encrypted == resource["encrypted"]
    assert throughput_mode == resource["throughput_mode"]
    assert availability_zone_name == resource["availability_zone_name"]
    assert backup == resource["backup"]
    assert tags == resource["tags"]

    # Actual Create file system resource
    ret = await hub.states.aws.efs.file_system.present(
        ctx,
        name=file_system_name,
        creation_token=creation_token,
        resource_id=resource_id,
        performance_mode=performance_mode,
        encrypted=encrypted,
        throughput_mode=throughput_mode,
        provisioned_throughput_in_mibps=provisioned_throughput_in_mibps,
        availability_zone_name=availability_zone_name,
        backup=backup,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (f"Created aws.efs.file_system '{file_system_name}'",)

    resource = ret.get("new_state")
    assert creation_token == resource["creation_token"]
    assert performance_mode == resource["performance_mode"]
    assert encrypted == resource["encrypted"]
    assert throughput_mode == resource["throughput_mode"]
    assert availability_zone_name == resource["availability_zone_name"]
    assert backup == resource["backup"]
    assert tags == resource["tags"]

    # Populate ID of previously created file system
    resource_id = resource["resource_id"]

    # Describe file system
    ret = await hub.states.aws.efs.file_system.describe(ctx)
    resource = ret.get(resource_id).get("aws.efs.file_system.present")
    resource_map = dict(ChainMap(*resource))
    assert creation_token == resource_map.get("creation_token")
    assert performance_mode == resource_map.get("performance_mode")
    assert encrypted == resource_map.get("encrypted")
    assert throughput_mode == resource_map.get("throughput_mode")
    assert availability_zone_name == resource_map.get("availability_zone_name")
    assert backup == resource_map.get("backup")
    assert tags == resource_map.get("tags")

    # Test-run file system update with no changes
    ret = await hub.states.aws.efs.file_system.present(
        test_ctx,
        name=file_system_name,
        creation_token=creation_token,
        resource_id=resource_id,
        performance_mode=performance_mode,
        encrypted=encrypted,
        throughput_mode=throughput_mode,
        provisioned_throughput_in_mibps=provisioned_throughput_in_mibps,
        availability_zone_name=availability_zone_name,
        backup=backup,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (
        f"aws.efs.file_system '{file_system_name}' already exists",
    )
    resource = ret.get("new_state")
    assert creation_token == resource["creation_token"]
    assert performance_mode == resource["performance_mode"]
    assert encrypted == resource["encrypted"]
    assert throughput_mode == resource["throughput_mode"]
    assert availability_zone_name == resource["availability_zone_name"]
    assert backup == resource["backup"]
    assert tags == resource["tags"]

    # Actual file system update with no changes
    ret = await hub.states.aws.efs.file_system.present(
        ctx,
        name=file_system_name,
        creation_token=creation_token,
        resource_id=resource_id,
        performance_mode=performance_mode,
        encrypted=encrypted,
        throughput_mode=throughput_mode,
        provisioned_throughput_in_mibps=provisioned_throughput_in_mibps,
        availability_zone_name=availability_zone_name,
        backup=backup,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (
        f"aws.efs.file_system '{file_system_name}' already exists",
    )
    resource = ret.get("new_state")
    assert creation_token == resource["creation_token"]
    assert performance_mode == resource["performance_mode"]
    assert encrypted == resource["encrypted"]
    assert throughput_mode == resource["throughput_mode"]
    assert availability_zone_name == resource["availability_zone_name"]
    assert backup == resource["backup"]
    assert tags == resource["tags"]

    # Test-run file system update with backup and throughput_mode changed
    ret = await hub.states.aws.efs.file_system.present(
        test_ctx,
        name=file_system_name,
        creation_token=creation_token,
        resource_id=resource_id,
        performance_mode=performance_mode,
        encrypted=encrypted,
        throughput_mode=updated_throughput_mode,
        provisioned_throughput_in_mibps=provisioned_throughput_in_mibps,
        availability_zone_name=availability_zone_name,
        backup=updated_backup,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (f"Would update aws.efs.file_system '{file_system_name}'",)
    resource = ret.get("new_state")
    assert creation_token == resource["creation_token"]
    assert performance_mode == resource["performance_mode"]
    assert encrypted == resource["encrypted"]
    assert updated_throughput_mode == resource["throughput_mode"]
    assert (
        provisioned_throughput_in_mibps == resource["provisioned_throughput_in_mibps"]
    )
    assert availability_zone_name == resource["availability_zone_name"]
    assert updated_backup == resource["backup"]
    assert tags == resource["tags"]

    # Actual file system update with throughput_mode and backup changed
    ret = await hub.states.aws.efs.file_system.present(
        ctx,
        name=file_system_name,
        creation_token=creation_token,
        resource_id=resource_id,
        performance_mode=performance_mode,
        encrypted=encrypted,
        throughput_mode=updated_throughput_mode,
        provisioned_throughput_in_mibps=provisioned_throughput_in_mibps,
        availability_zone_name=availability_zone_name,
        backup=updated_backup,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (
        f"Updated ThroughputMode for aws.efs.file_system '{file_system_name}'",
        f"Updated Backup policy for aws.efs.file_system '{file_system_name}'",
    )
    resource = ret.get("new_state")
    assert creation_token == resource["creation_token"]
    assert performance_mode == resource["performance_mode"]
    assert encrypted == resource["encrypted"]
    assert availability_zone_name == resource["availability_zone_name"]
    assert updated_backup == resource["backup"]
    assert tags == resource["tags"]

    # Test run file system update with tags changed
    ret = await hub.states.aws.efs.file_system.present(
        test_ctx,
        name=file_system_name,
        creation_token=creation_token,
        resource_id=resource_id,
        performance_mode=performance_mode,
        encrypted=encrypted,
        throughput_mode=updated_throughput_mode,
        provisioned_throughput_in_mibps=provisioned_throughput_in_mibps,
        availability_zone_name=availability_zone_name,
        backup=updated_backup,
        tags=updated_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (f"Would update aws.efs.file_system '{file_system_name}'",)
    resource = ret.get("new_state")

    assert creation_token == resource["creation_token"]
    assert performance_mode == resource["performance_mode"]
    assert encrypted == resource["encrypted"]
    assert updated_throughput_mode == resource["throughput_mode"]
    assert availability_zone_name == resource["availability_zone_name"]
    assert (
        provisioned_throughput_in_mibps == resource["provisioned_throughput_in_mibps"]
    )
    assert updated_backup == resource["backup"]
    assert updated_tags == resource["tags"]

    # Actual file system update with tags changed
    ret = await hub.states.aws.efs.file_system.present(
        ctx,
        name=file_system_name,
        creation_token=creation_token,
        resource_id=resource_id,
        performance_mode=performance_mode,
        encrypted=encrypted,
        throughput_mode=updated_throughput_mode,
        provisioned_throughput_in_mibps=provisioned_throughput_in_mibps,
        availability_zone_name=availability_zone_name,
        backup=updated_backup,
        tags=updated_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (
        "Update tags:",
        "Add [[{'Key': 'id', 'Value': 'fs-1'}]]",
        "Remove []",
    )
    resource = ret.get("new_state")
    assert creation_token == resource["creation_token"]
    assert performance_mode == resource["performance_mode"]
    assert encrypted == resource["encrypted"]
    assert updated_throughput_mode == resource["throughput_mode"]
    assert (
        provisioned_throughput_in_mibps == resource["provisioned_throughput_in_mibps"]
    )
    assert availability_zone_name == resource["availability_zone_name"]
    assert updated_backup == resource["backup"]
    assert updated_tags == resource["tags"]

    # Test-run file system deletion
    ret = await hub.states.aws.efs.file_system.absent(
        test_ctx, name=file_system_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == (f"Would delete aws.efs.file_system '{file_system_name}'",)
    resource = ret.get("old_state")
    assert creation_token == resource["creation_token"]
    assert performance_mode == resource["performance_mode"]
    assert encrypted == resource["encrypted"]
    assert updated_throughput_mode == resource["throughput_mode"]
    assert (
        provisioned_throughput_in_mibps == resource["provisioned_throughput_in_mibps"]
    )
    assert availability_zone_name == resource["availability_zone_name"]
    assert updated_backup == resource["backup"]
    assert updated_tags == resource["tags"]

    # Delete non-existent file system
    ret = await hub.states.aws.efs.file_system.absent(
        ctx, name="dummy", resource_id="dummy"
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == (f"aws.efs.file_system 'dummy' already absent",)

    # Delete file system
    ret = await hub.states.aws.efs.file_system.absent(
        ctx, name=file_system_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == (f"Deleted aws.efs.file_system '{file_system_name}'",)
    resource = ret.get("old_state")
    assert creation_token == resource["creation_token"]
    assert performance_mode == resource["performance_mode"]
    assert encrypted == resource["encrypted"]
    assert updated_throughput_mode == resource["throughput_mode"]
    assert (
        provisioned_throughput_in_mibps == resource["provisioned_throughput_in_mibps"]
    )
    assert availability_zone_name == resource["availability_zone_name"]
    assert updated_backup == resource["backup"]
    assert updated_tags == resource["tags"]
